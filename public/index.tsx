import Layout from '../component/Layout';
import Tools from '../component/Tools';
import Working from '../component/Working';
import Curriculum from "../component/Curriculum";
import CarouselGitlab from "../component/CarouselGitlab";
import CarouselGithub from "../component/CarouselGithub";

import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

const Home = () => {
    return (
        <Layout>
            <Tools />
            <Working />
            <Curriculum />
            <CarouselGitlab />
            <CarouselGithub />
        </Layout>
    )
}

export const getStaticProps = async ({ locale }: any) => ({
    props: {
        ...await serverSideTranslations(locale, ['firstpage']),
    },
})
export default Home
