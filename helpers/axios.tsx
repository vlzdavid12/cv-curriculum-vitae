import {makeUseAxios, UseAxios} from "axios-hooks";
import axios from "axios";

export const useAxiosGitlab: UseAxios = makeUseAxios({
    axios: axios.create({ baseURL: 'https://gitlab.com/api/v4' })
})

export const useAxiosGithub: UseAxios = makeUseAxios({
    axios: axios.create({ baseURL: 'https://api.github.com',
                          headers: { 'Authorization': process.env.NEXT_PUBLIC_API_GITHUP }})
})






