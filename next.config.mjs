/** @type {import('next').NextConfig} */
import pkg from './next-i18next.config.js';
import withImages from 'next-images';

const { i18n } = pkg;
const nextConfig = {
  i18n,
  reactStrictMode: true,
};

export default {
  ...nextConfig,
  withImages: withImages({
    webpack(config, { isServer }) {
      // Add support for importing SVG files
      config.module.rules.push({
        test: /\.svg$/,
        issuer: {
          and: [/\.(js|ts)x?$/],
        },
        use: ['@svgr/webpack'],
      });

      // Add support for importing images (PNG, JPG, JPEG, GIF)
      config.module.rules.push({
        test: /\.(png|jpe?g|gif)$/i,
        issuer: {
          and: [/\.(js|ts)x?$/],
        },
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192, // Inlines small images as data URLs for better performance
              fallback: 'file-loader',
            },
          },
        ],
      });

      return config;
    },
  }),
};


