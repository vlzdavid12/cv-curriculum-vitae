import React, { useEffect, useRef } from 'react';
import { gsap } from "gsap";
import { CustomEase } from 'gsap/dist/CustomEase'
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import Image from "next/image";
import Link from "next/link";
import UdemyImage from "../public/udemy.png"
import ImageSaberBar from "../public/barranav.svg"

gsap.registerPlugin(ScrollTrigger);
gsap.registerPlugin(CustomEase);

import { useTranslation } from 'next-i18next'

const Curriculum = () => {

    const BarNav = useRef(null);

    const { t } = useTranslation('firstpage');

    useEffect(() => {

        gsap.to(BarNav.current, {
            duration: 2.5, ease:
                CustomEase.create("custom", "M0,0,C0.127,0.382,0.286,0.674,0.446,0.822,0.64,1.002,0.815,-0.011,1,-0.012"), y: 20, repeat: -1,
        });


    }, [ BarNav]);

    return (
        <>
            <div style={{ display: 'flex', alignItems: 'center', alignContent: 'center', justifyContent: 'center', padding: '10px' }} ref={BarNav}>
            <Image  alt='img1' src={ImageSaberBar} width={1000} height={130} />
            </div>
            <div className="curriculum" >
                <div className="column-curriculum"  >
                    <h2 className="title-curriculum">{t('titles.title_1')}</h2>
                    <ul style={{ listStyle: 'none', margin: '0px', padding: '0px' }}>
                        <li>
                            <h2 className="title-curriculum">{t('titles.title_2')}</h2>
                            <h3 className="subtitle-curriculum">Trainingym.</h3>
                            <p className="paragraph">{t('business.description_4')}</p>
                            <Link href="https://trainingym.com/en/" legacyBehavior><a>www.trainingym.com</a></Link>
                        </li>
                        <li>
                            <h3 className="subtitle-curriculum">Cloudberry o Diens.</h3>
                            <p className="paragraph">{t('business.description_3')}</p>
                            <Link href="https://cloudberry.com.co/" legacyBehavior ><a>www.cloudberry.com.co</a></Link>
                        </li>
                        <li>
                            <h3 className="subtitle-curriculum">Pixel Group</h3>
                            <p className="paragraph">{t('business.description_2')}</p>
                            <Link href="https://pixelgp.com/" legacyBehavior ><a>www.pixelgp.com</a></Link>
                        </li>
                    </ul>
                </div>
                <div className="column-curriculum">

                    <h2 className="title-curriculum">{t('titles.title_2')}</h2>
                    <h3 className="subtitle-curriculum">SingleClick.</h3>
                    <p className="paragraph">{t('business.description_5')}</p>
                    <Link href="https://singleclick.com.co/" legacyBehavior ><a>www.singleclick.com.co</a></Link>

                    <h3 className="subtitle-curriculum">Xisfo Fintech</h3>
                    <p className="paragraph">{t('business.description_6')}</p>
                    <Link href="https://xisfo.co/" legacyBehavior ><a>www.xisfo.co</a></Link>

                </div>
                <div className="column-curriculum">
                    <h2 className="title-curriculum">{t('titles.title_3')}</h2>
                    <p className="paragraph">Administración y Tecnología CSAT</p>
                    <p className="paragraph">Artes Gráficas y Aplicativos Web</p>
                    <p className="paragraph">{t('student.description_1')}</p>
                    <Image src={UdemyImage} alt="udemy" width={125} height={50} />
                </div>
            </div>
        </>)
}
export default Curriculum;
