import React from 'react';
import Head from 'next/head'
import Header from './Header'
import Footer from "./Footer";


const Layout = ({ children }:any) => {
    return (
        <div>
            <Head>
                <title>HV: David Valenzuela Pardo</title>

                <meta name="viewport" content="width=device-width, initial-scale=1.0" />

                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css"
                      integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w=="
                      crossOrigin="anonymous" referrerPolicy="no-referrer"/>
                     

            </Head>
            <div style={{maxWidth: '1150px', margin: 'auto'}} >
                <Header/>
                <main>
                    {children}
                </main>
                <Footer/>
            </div>
        </div>
    )
}
export default Layout;