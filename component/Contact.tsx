import React, {useState, useEffect, useRef} from 'react'
import Image from "next/image";
import {gsap} from "gsap";
import {CustomEase} from "gsap/dist/CustomEase";
import {useFormik} from 'formik';
import * as yup from 'yup';
import * as emailjs from 'emailjs-com';

import File_1 from '../public/contact/file_1.svg';
import File_2 from '../public/contact/file_2.svg';
import File_3 from '../public/contact/file_3.svg';
import File_4 from '../public/contact/file_4.svg';
import File_5 from '../public/contact/file_5.svg';

const Contact = ({show, hideModal}: any) => {

    const [getForm, setForm] = useState(false);

    const file_1 = useRef(null);
    const file_2 = useRef(null);
    const file_3 = useRef(null);
    const file_4 = useRef(null);


    const validationSchema = yup.object({
        name: yup.string()
            .required('Ingrese el nombre completo.'),
        email: yup
            .string()
            .email('Ingrese el correo correctamente.')
            .required('Ingresa el correo electrónico'),
        comments: yup
            .string()
            .required('Ingresa el comentario o sugerencia')

    });

    const formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            comments: '',
        },
        validationSchema: validationSchema,
        onSubmit: (object: { name: string, email: string, comments: string }, {resetForm}) => {
            try {
                const user_ID = 'user_JhJ4pzTzuKVHXegHY9W25';
                const service_ID = 'default_service';
                const template_ID = 'template_b4u80e9';
                emailjs.send(service_ID, template_ID, object, user_ID)
                    .then(() => {
                        setForm(true);
                        resetForm();
                        setTimeout(() => {
                            setForm(false);
                        }, 4000)
                    });
            } catch (error) {
                console.error(error)
            }
        }
    })


    const animate = CustomEase.create("custom", "M0,0 C0.126,0.382 0.282,0.674 0.44,0.822 0.632,1.002 0.818,0.027 1,0.026 ");

    useEffect(() => {
        if (show) {
            gsap.from(file_1.current, {
                y: -20,
                rotate: -10,
                duration: 2.5,
                ease: animate,
                repeat: -1,
            })

            gsap.from(file_2.current, {
                y: -20,
                rotate: 10,
                duration: 2.5,
                ease: animate,
                repeat: -1,
            })


            gsap.from(file_3.current, {
                y: 50,
                duration: 2.5,
                ease: animate,
            })

            gsap.from(file_4.current, {
                y: -20,
                rotate: 10,
                duration: 2.5,
                ease: animate,
                repeat: -1,
            })
        }

    }, [show])


    const Alert = (message: string) => <div className="error-invalid">{message}</div>;

    const Success = (message: string) => <div className="success-send">{message}</div>;

    return (show && (
        <div className="modal-form">
            <div className="modal-shadow"  onClick={() => hideModal()}></div>
            <div ref={file_3} className="form">
                <form onSubmit={formik.handleSubmit}>
                    <input type="text"
                           id="name"
                           name="name"
                           value={formik.values.name}
                           onChange={formik.handleChange}
                           className="input-form"
                           placeholder="Nombre Completo..."/>
                    {
                        formik.touched.name && formik.errors.name ? Alert(formik.errors.name) : ''
                    }
                    <input type="text"
                           id="email"
                           name="email"
                           value={formik.values.email}
                           onChange={formik.handleChange}
                           className="input-form"
                           placeholder="Correo electrónico..."/>
                    {
                        formik.touched.email && formik.errors.email ? Alert(formik.errors.email) : ''
                    }
                    <textarea
                        id="comments"
                        name="comments"
                        value={formik.values.comments}
                        onChange={formik.handleChange}
                        className="textarea-form"
                        placeholder="Comentario o sugerencia">
                        </textarea>
                    {
                        formik.touched.comments && formik.errors.comments ? Alert(formik.errors.comments) : ''
                    }
                    <input className="submit-form" type="submit" value="Enviar"/>
                </form>
                {getForm && (<div>{Success('Mensage enviado...')}</div>)}
                <div ref={file_1} className="file_1">
                    <Image alt='file_1' src={File_1} width={200} height={200}/>
                </div>
                <div ref={file_2} className="file_2">
                    <Image alt="file2" src={File_2} width={250} height={250}/>
                </div>
                <div className="file_3">
                    <Image alt="file3" src={File_3} width={250} height={250}/>
                </div>
                <div ref={file_4} className="file_4">
                    <Image alt="file4" src={File_4} width={200} height={200}/>
                </div>
                <div className="file_5">
                    <Image alt="file5" src={File_5} width={200} height={200}/>
                </div>
            </div>
        </div>
    ));

}
export default Contact;