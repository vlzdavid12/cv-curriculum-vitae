import React, {useRef, useEffect, useState} from 'react';
import gsap from 'gsap';
import {ScrollTrigger} from "gsap/dist/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

import Image from "next/image";
import toolsTree from "../public/tools3.png"
const Working = () => {

    const revealRef_1 = useRef(null);
    const revealRef_2 = useRef(null)

    useEffect(() => {
        gsap.from(revealRef_1.current, {
            scrollTrigger: {
                trigger: '.hv-tools',
                start: "top center",
                end: "+=200", // end after scrolling 500px beyond the start
                scrub: 1, // smooth scrubbing, takes 1 second to "catch up" to the scrollbar
                // markers: {startColor: "white", endColor: "white", fontSize: "18px", fontWeight: "bold", indent: 20},
                //markers: true,
            },
            ease: 'none',
            x: -200,
            duration: 1,
            opacity: 0,
        });

        gsap.from(revealRef_2.current, {
            scrollTrigger: {
                trigger: '.hv-tools',
                start: "top center",
                end: "+=200", // end after scrolling 500px beyond the start
                scrub: 1, // smooth scrubbing, takes 1 second to "catch up" to the scrollbar
                //markers: true,
            },
            ease: 'none',
            x: 200,
            duration: 1,
            opacity: 0,
        })


    }, []);

    return (<div className="tools-adobe" >
        <div className="image" ref={revealRef_1}>
            <Image src={toolsTree}  width={290}
                   height={100} alt="tools-adobe" />
        </div>
        <div style={{padding: '10px'}}  ref={revealRef_2}>
            <h3 className="title-design" >Diseño</h3>
            <p>Adobe Photoshop, Adobe Ilustreitor, Adobe After Effects</p>
            <p>Esencial</p>
        </div>
    </div>)
}
export default Working;
