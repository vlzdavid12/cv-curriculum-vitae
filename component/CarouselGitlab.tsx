import React from "react";
import Slider from "react-slick";
import Image from "next/image";
import gitLabImage from "../public/gitlab.png";
import {useAxiosGitlab} from "../helpers/axios";
import {GitLab} from "../interface/interface";

const CarouselGitlab = () =>{

    const [{ data, loading, error } ] = useAxiosGitlab<GitLab[]>('/users/5589477/projects')

    const settings = {
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true,
                    arrows:false,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 1,
                    arrows:false,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows:false,
                    dots: false
                }
            }
        ]
    };

    if (loading) return null;
    if (error) return null;

    return(
            <div className="carousel"  >
                <Image alt="image1" src={gitLabImage} width={100} height={40} />
                <Slider {...settings}>
                    {data? data.map((item:GitLab)=>(
                            <div key={item.id}>
                                <div className="container">
                                    <a href={item.web_url} target="_blank" className="link"><h3 className="title">{item.name.length >= 32 ? item.name.slice(0, 32) + " ..." : item.name}</h3></a>
                                    <p className="description">{item.description}</p>
                                </div>
                            </div>
                        ))
                    : ''}
                </Slider>
            </div>
    )
}
export default CarouselGitlab