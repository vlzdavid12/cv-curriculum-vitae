import React, {useState} from 'react';
import Image from "next/image";
import Link from "next/link";
import Contact from "./Contact";
import behance from '../public/behance.png'
import gitLabFooter from '../public/gitlab-footer.png';
import imageContact from "../public/email_icon.png";

import { useTranslation } from 'next-i18next'


const Footer = () => {
    const  [getModal, setModal]= useState<boolean>(false);
    const modal = (value: boolean) =>{
            setModal(value);
    }

    const hideModal  = () =>{
       setModal(false)
    }

    const { t } = useTranslation('firstpage');

    return (
        <>
            <div className="hv-footer-description" >
                <div style={{ display: 'flex',}}>
                    <div>
                        <Link  href="https://www.behance.net/vlzdavid127aa9" legacyBehavior ><a><Image alt='img2' src={behance} width={40} height={40}/></a></Link>
                    </div>
                    <div style={{marginLeft: '5px'}}>
                        <Link  href="https://www.behance.net/vlzdavid127aa9" legacyBehavior ><a><h4 style={{color: '#fff', margin: '0px'}}>Behance</h4>
                        <span style={{color: '#fff', margin: '0px', fontSize: '12px'}}>www.behance.net</span></a>
                        </Link>
                    </div>
                </div>
                <div>
                    <Link href="https://gitlab.com/vlzdavid12/cv-curriculum-vitae" legacyBehavior  ><a style={{cursor:'pointer', textAlign: 'center', display: 'block'}} >
                        <Image alt='img1' src={gitLabFooter} width={50} height={55}  />
                        <p style={{textAlign: 'center', color: '#fff', fontSize: '10px', margin: '0px'}}>{t('repository')}</p>
                    </a>
                    </Link>
                </div>
                <div>
                    <div style={{display: 'block', textAlign: 'right'}}>
                        <a style={{cursor:'pointer', fontSize: '14px', color:'#fff', margin: '0px', padding: '0px'}} onClick={()=>modal(!getModal)}  >{t('contact')} <Image alt='image3' src={imageContact} width={24} height={20} /></a>
                    <p style={{color:'#fff',  fontSize: '12px', margin: '3px auto'}}>+57 321 348 7458</p>
                    </div>
                </div>
            </div>
            <div className="hv-footer">
            </div>
            <Contact show={getModal} hideModal={()=>hideModal()} />
        </>
    )

}
export default Footer
