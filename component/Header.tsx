import React, { useRef } from 'react';
import {NextRouter, useRouter} from "next/router";
import {useAxiosGithub} from "../helpers/axios";
import {GitHubUser} from "../interface/interface";
import { useTranslation } from 'next-i18next'
import flagSpanish from "../public/spanish.png";
import flagEnglish from "../public/english.png";
import Image from "next/image";
import Link from "next/link";
import Linkeding from "../public/linkeding.png";

const Header = () => {
    const headerHVRef = useRef(null);
    const router: NextRouter = useRouter()
    const { t } = useTranslation('firstpage');

    const [{data, loading, error}] = useAxiosGithub<GitHubUser>('/users/valenzuela21')

    if (loading) return null;
    if (error) return null;

    return (
            <div className="hv-header" ref={headerHVRef}>
                <Link

                    href='/'
                    locale={router.locale === 'en' ? 'es' : 'en'}
                    legacyBehavior
                ><a className="btn-idioms">
                        {router.locale === 'es'?<Image src={flagEnglish} alt="english" width={30} height={22} />:<Image src={flagSpanish} alt="spanish" width={30} height={22} />}
                </a>
                </Link>
                <div  className="header-info">
                    <img src={data?.avatar_url} alt="david-valenzuela-pardo" className="img-avatar" />

                    <div className="info">
                        <div
                            className="box-linked"
                        >
                            <Link legacyBehavior href="https://www.linkedin.com/in/david-fernando-valenzuela-pardo-185711100" ><a><Image alt='image2' src={Linkeding} width={120} height={120} /></a></Link>
                        </div>
                    <h2 className="name-author">{data?.name}</h2>
                        <p className="name-info"> {t('profession')} <strong>Backend y Frontend</strong> <span className='hand'>👋</span></p>
                        <p className="name-info"> {t('description')}</p>
                    </div>
                </div>

            </div>
    )
}


export default Header
