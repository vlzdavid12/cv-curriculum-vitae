import React, {useRef, useEffect} from 'react';
import {gsap} from "gsap";

import {ScrollTrigger} from "gsap/dist/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

import Image from 'next/image'
import toolsFirst from '../public/tools.png'
import toolsSeconds from '../public/tools2.png'
import MouseImage from "../public/mouse.svg";


const Tools = () => {

   

    return (
        <>
        <div className="hv-tools">
        <div>
            <Image  src={toolsFirst}
                   alt="tools"
            />
        </div>
        <div>
            <Image src={toolsSeconds}
                   alt="tools"
            />
        </div>
    </div>
        </>)
}
export default Tools