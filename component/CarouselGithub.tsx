import React from "react";
import Slider from "react-slick";
import Image from "next/image";

import githubImage from "../public/githup.png"
import {useAxiosGithub} from "../helpers/axios";
import {GitHub} from "../interface/interface";

const CarouselGithub = () =>{

    const [{data, loading, error}] = useAxiosGithub<GitHub[]>('/users/valenzuela21/repos')

    if (loading) return null;
    if (error) return null;


    const settings = {
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    arrows:false,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 1,
                    infinite: true,
                    arrows:false,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows:false,
                    dots: false
                }
            }
        ]
    };


    return(
        <>
            <div className="carousel">
                <Image alt="image 2" src={githubImage} width={90} height={40} />
                <Slider {...settings}>
                    {data? data.map((item:GitHub)=>(
                        <div key={item.id}>
                            <div className="container">
                                <a href={item.html_url} target="_blank" className="link" ><h3 className="title">{item.name.length >= 32 ? item.name.slice(0, 32) + " ..." : item.name}</h3></a>
                                <p className="description">{item.description}</p>
                                <p className="language">{`Lenguage: ${item.language}`}</p>
                            </div>
                        </div>
                    )) : null}
                </Slider>
            </div>
        </>
    )
}
export default CarouselGithub
